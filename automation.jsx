
var doc = app.activeDocument;

var backgroundLayer = doc.layers.getByName("Background");
backgroundLayer.visible = true;

var folderNames = ["Icons", "Logos"];

for (var i = 0; i < folderNames.length; i++) {
    var folder = doc.layerSets.getByName(folderNames[i]);

    var outputFolder = new Folder(doc.path + "/" + folderNames[i]);
    if (!outputFolder.exists) outputFolder.create();

    for (var j = 0; j < folder.layers.length; j++) {
        var layer = folder.layers[j];
        
        layer.visible = true;

        var fileName = layer.name + ".png";
        doc.saveAs(new File(outputFolder + "/" + fileName), new PNGSaveOptions(), true);

        layer.visible = false
    }
}

//doc.close(SaveOptions.DONOTSAVECHANGES);